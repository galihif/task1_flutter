import 'package:flutter/material.dart';
import 'package:task1_flutter/data/MainRepository.dart';
import 'package:task1_flutter/data/model/LoginModel.dart';
import 'package:task1_flutter/ui/Profile.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String email = "";
  String password = "";
  String status = "";
  LoginModel? res;

  void setEmail(String mEmail){
    setState(() {
      email = mEmail;
    });
  }

  void setPassword(String mPassword){
    setState(() {
      password = mPassword;
    });
  }

  void setStatus(String mStatus){
    setState(() {
      status = mStatus;
    });
  }

  void login() async{
    res = await MainRepository().login(this.email,this.password);
    if(res!.meta!=null){
      setState(() {
        status = res!.meta.message;
      });
    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text("Login"),
            const Text("Silahkan masukkan email dan password"),
            Text("$email"),
            Text("$password"),
            Text("$status"),
            TextFormField(
              decoration: const InputDecoration(
                hintText: 'Enter your email',
              ),
              onChanged: (String? value){
                setEmail(value.toString());
              },
              validator: (String? value) {
                if (value == null || value.isEmpty) {
                  return 'Please fill email';
                }
                return null;
              },
            ),
            TextFormField(
              decoration: const InputDecoration(
                hintText: 'Enter your password',
              ),
              obscureText: true,
              onChanged: (String? value){
                setPassword(value.toString());
              },
              validator: (String? value) {
                if (value == null || value.isEmpty) {
                  return 'Please fill password';
                }
                return null;
              },
            ),
            ElevatedButton(
              onPressed:(){
                // Navigator.of(context).push(
                //   MaterialPageRoute(builder: (BuildContext context){
                //     return const Profile();
                //   })
                // );
                login();
              },
              child: const Text("Login"),
            )
          ],
        )
    );
  }
}
