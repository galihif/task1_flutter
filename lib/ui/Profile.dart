import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepOrange,
        title: const Text("Akun")
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Header(),
          Ktp(),
          Bank()
        ],
      ),
    );
  }
}

class Header extends StatelessWidget {
  const Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Image.network(
                'https://www.greenscene.co.id/wp-content/uploads/2022/01/Naruto-1-696x497.jpg',
              height: 72.0,
              width: 72.0,
            ),
            Column(
              children: [
                Text("Nama user"),
                Text("Email user"),
              ],
            )
          ],
        ),
        Text("Kota user"),
        Text("Pekerjaan user")
      ],
    );
  }
}

class Ktp extends StatelessWidget {
  const Ktp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children:  [
        Text("KTP Name", style: TextStyle(fontWeight: FontWeight.bold)),
        Text("John doe"),
        Text("KTP Number", style: TextStyle(fontWeight: FontWeight.bold)),
        Text("2323223"),
        Text("KTP Domicile", style: TextStyle(fontWeight: FontWeight.bold)),
        Text("Jakarta"),
        Text("Birthdate", style: TextStyle(fontWeight: FontWeight.bold)),
        Text("28-9-2000"),

      ],
    );
  }
}

class Bank extends StatelessWidget {
  const Bank({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children:  [
        Text("Balance", style: TextStyle(fontWeight: FontWeight.bold)),
        Text("John doe"),
        Text("Bank Account Number", style: TextStyle(fontWeight: FontWeight.bold)),
        Text("2323223"),
        Text("Bank Account Name", style: TextStyle(fontWeight: FontWeight.bold)),
        Text("Jakarta"),
        Text("Bank Name", style: TextStyle(fontWeight: FontWeight.bold)),
        Text("28-9-2000"),

      ],
    );
  }
}



