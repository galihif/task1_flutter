import 'dart:convert';

import 'model/LoginModel.dart';
import 'package:http/http.dart' as http;

class MainRepository{
  String BASE_URL = "https://api-staging.serbaseleb.com";

  Future<LoginModel> login(String email, String password) async{
    var client = http.Client();
    var url = Uri.parse(BASE_URL+"/login");
    Map data = {
      'email': email,
      'password': password
    };
    var response = await client.post(
      url,
      body: json.encode(data),
      headers: {
        'Content-Type': 'application/json'
      }
    );
    print("${response.statusCode}");
    print("${response.body}");
    return loginModelFromJson(response.body);
  }
}