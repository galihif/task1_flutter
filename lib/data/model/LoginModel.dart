// To parse this JSON data, do
//
//     final loginModel = loginModelFromJson(jsonString);

import 'dart:convert';

LoginModel loginModelFromJson(String str) => LoginModel.fromJson(json.decode(str));

String loginModelToJson(LoginModel data) => json.encode(data.toJson());

class LoginModel {
  LoginModel({
    required this.meta,
    this.data,
  });

  Meta meta;
  Data? data;

  factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
    meta: Meta.fromJson(json["meta"]),
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "meta": meta.toJson(),
    "data": data?.toJson(),
  };
}

class Data {
  Data({
    required this.userId,
    required this.userType,
    required this.userCode,
    required this.email,
    required this.name,
    required this.isVerified,
    required this.photoProfile,
    required this.token,
  });

  int userId;
  int userType;
  String userCode;
  String email;
  String name;
  bool isVerified;
  String photoProfile;
  String token;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    userId: json["user_id"],
    userType: json["user_type"],
    userCode: json["user_code"],
    email: json["email"],
    name: json["name"],
    isVerified: json["is_verified"],
    photoProfile: json["photo_profile"],
    token: json["token"],
  );

  Map<String, dynamic> toJson() => {
    "user_id": userId,
    "user_type": userType,
    "user_code": userCode,
    "email": email,
    "name": name,
    "is_verified": isVerified,
    "photo_profile": photoProfile,
    "token": token,
  };
}

class Meta {
  Meta({
    required this.code,
    required this.message,
    required this.error,
  });

  int code;
  String message;
  bool error;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
    code: json["code"],
    message: json["message"],
    error: json["error"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "message": message,
    "error": error,
  };
}
